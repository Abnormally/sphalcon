let mix = require('laravel-mix');
let path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .webpackConfig({
        output: {
            path: path.resolve(__dirname, 'public'),
            chunkFilename: 'js/chunk/[id].js',
            publicPath: '/',
        },
        resolve: {
            alias: {
                "$view": path.resolve(__dirname, 'resources/assets/front/pages'),
                "$part": path.resolve(__dirname, 'resources/assets/front/components'),
                "$dept": path.resolve(__dirname, 'resources/assets/front/control'),
                "$plug": path.resolve(__dirname, 'resources/assets/front/plugins'),
            }
        },
        module: {
            rules: [
                {
                    test: /\.styl$/,
                    use: [
                        'style-loader',
                        'css-loader',
                        'stylus-loader',
                    ],
                },
                // {
                //     test: /\.s(c|a)ss$/,
                //     use: [
                //         'style-loader',
                //         'css-loader',
                //         'sass-loader',
                //     ],
                // },
                {
                    test: /\.less$/,
                    use: [
                        'style-loader',
                        'css-loader',
                        'less-loader',
                    ],
                },
            ],
            loaders: [
                {
                    test: /\.styl$/,
                    loader: 'stylus-loader'
                },
                {
                    test: /\.s(c|a)ss$/,
                    loader: 'sass-loader'
                },
                {
                    test: /\.less$/,
                    loader: 'less-loader'
                },
            ]
        },
    })

    .js('resources/assets/front/app.js', '/js');
