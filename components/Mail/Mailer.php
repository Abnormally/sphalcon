<?php

namespace Components\Mail;

use Phalcon\Di\FactoryDefault;

/**
 * Class Mailer
 * @package Components\Mail
 *
 * @property \Phalcon\Di $di
 */
class Mailer
{
    
    public function __construct( $di = null )
    {
        $this->di = $di ? $di : FactoryDefault::getDefault();
    }
    
}