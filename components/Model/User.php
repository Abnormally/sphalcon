<?php

namespace Components\Model;

use Components\Model\Traits\Timestampable;
use Components\Model\Traits\SoftDeletable;

class User extends Model
{
    use Timestampable, SoftDeletable;
    
    protected $id;
    public    $email;
    public    $password;
    public    $token;
    protected $nickname;
    protected $activated;
    
    /**
     * By every request, phalcon will always pull this function
     * as basis to know what is the table's name.
     *
     * @return string
     */
    public function getSource()
    {
        return 'users';
    }
    
    public $public_params = [
        'id',
        'nickname',
        'created_at',
        'updated_at',
    ];
    
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set the name field.
     *
     * @param string $nickname setting the name of the user
     * @return mixed
     */
    public function setNickname( $nickname )
    {
        $this->nickname = $nickname;
        
        return $this;
    }
    
    /**
     * Get the user's name.
     *
     * @return string
     */
    public function getNickname()
    {
        return $this->nickname;
    }
    
    /**
     * Set the email.
     *
     * @param string $email
     */
    public function setEmail( $email )
    {
        $this->email = $email;
        
        return $this;
    }
    
    /**
     * Get the user's email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    /**
     * Set the column activated in the table as boolean.
     *
     * @param bool $bool a boolean value to be based if activated or not
     * @return mixed
     */
    public function setActivated( $bool )
    {
        $this->activated = (int) $bool;
        
        return $this;
    }
    
    /**
     * To know if the account is activated.
     *
     * @return bool
     */
    public function getActivated()
    {
        return (bool) $this->activated;
    }
    
    /**
     * Get user public information
     *
     * @param array $appends
     * @return array
     */
    public function publicInfo( ...$appends )
    {
        return $this->toArray( array_merge( $this->public_params, $appends ) );
    }
    
}
