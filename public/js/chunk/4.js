webpackJsonp([4],{

/***/ 352:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(359)
}
var normalizeComponent = __webpack_require__(45)
/* script */
var __vue_script__ = __webpack_require__(361)
/* template */
var __vue_template__ = __webpack_require__(362)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-ebde54cc"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/front/pages/auth/SignUp.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-ebde54cc", Component.options)
  } else {
    hotAPI.reload("data-v-ebde54cc", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 359:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(360);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(124)("ae6dfcd6", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-ebde54cc\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./SignUp.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-ebde54cc\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./SignUp.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 360:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(60)(false);
// imports


// module
exports.push([module.i, "\n.abn-width-fix[data-v-ebde54cc] {\n    max-width: 50rem;\n}\n", ""]);

// exports


/***/ }),

/***/ 361:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });


/* harmony default export */ __webpack_exports__["default"] = ({
    name: "abn-sign-up",
    data: function data() {
        var _this = this;

        return {
            valid: false,
            sending: false,
            emailValidate: this.$store.getters['tech/emailValidate'],
            url: this.$store.getters['api/url'],
            nickname: {
                value: '',
                rules: [function (v) {
                    return !!v || 'Никнэйм обязателен';
                }, function (v) {
                    return v.length <= 100 || 'Никнэйм не может быть более ста символов';
                }, function (v) {
                    return v.length >= 6 || 'Никнэйм не может быть менее шести символов';
                }]
            },
            email: {
                value: '',
                rules: [function (v) {
                    return !!v || 'Адрес почты обязателен';
                }, function (v) {
                    return v.length <= 100 || 'Адрес почты не может быть более ста символов';
                }, function (v) {
                    return v.length >= 6 || 'Адрес почты не может быть менее шести символов';
                }, function (v) {
                    return _this.emailValidate(v) || 'Введите валидный aдрес почты ';
                }]
            },
            password: {
                value: '',
                rules: [function (v) {
                    return !!v || 'Пароль обязятелен';
                }, function (v) {
                    return v.length >= 6 || 'Пароль не может быть менее шести символов';
                }, function (v) {
                    return v.length <= 100 || 'Пароль не может быть более ста символов';
                }]
            }
        };
    },

    methods: {
        sendRegistration: function sendRegistration() {
            var _this2 = this;

            if (this.isDisabled) return;
            this.sending = true;

            var data = {
                nickname: this.nickname.value,
                email: this.email.value,
                password: this.password.value
            };

            var url = this.url('user.register');

            this.$http.post(url, data).then(function (response) {
                _this2.$log('No errors');
                _this2.$log(response);
            }).catch(function (error) {
                _this2.$log(error.response ? error.response : error);
            }).finally(function () {
                _this2.sending = false;
            });
        }
    },
    computed: {
        isDisabled: function isDisabled() {
            return !this.valid || this.sending;
        }
    }
});

/***/ }),

/***/ 362:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    { staticClass: "text-sm-center abn-width-fix" },
    [
      _c(
        "v-card",
        [
          _c(
            "v-card-text",
            [
              _c(
                "v-container",
                { attrs: { "grid-list-md": "" } },
                [
                  _c(
                    "v-form",
                    {
                      model: {
                        value: _vm.valid,
                        callback: function($$v) {
                          _vm.valid = $$v
                        },
                        expression: "valid"
                      }
                    },
                    [
                      _c("v-text-field", {
                        attrs: {
                          rules: _vm.nickname.rules,
                          label: "Ник",
                          required: "",
                          autofocus: ""
                        },
                        model: {
                          value: _vm.nickname.value,
                          callback: function($$v) {
                            _vm.$set(_vm.nickname, "value", $$v)
                          },
                          expression: "nickname.value"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-text-field", {
                        attrs: {
                          rules: _vm.email.rules,
                          label: "E-mail",
                          required: ""
                        },
                        model: {
                          value: _vm.email.value,
                          callback: function($$v) {
                            _vm.$set(_vm.email, "value", $$v)
                          },
                          expression: "email.value"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-text-field", {
                        attrs: {
                          rules: _vm.password.rules,
                          type: "password",
                          label: "Пароль",
                          required: ""
                        },
                        on: {
                          keyup: function($event) {
                            if (
                              !("button" in $event) &&
                              _vm._k(
                                $event.keyCode,
                                "enter",
                                13,
                                $event.key,
                                "Enter"
                              )
                            ) {
                              return null
                            }
                            return _vm.sendRegistration($event)
                          }
                        },
                        model: {
                          value: _vm.password.value,
                          callback: function($$v) {
                            _vm.$set(_vm.password, "value", $$v)
                          },
                          expression: "password.value"
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-card-actions",
            [
              _c("v-spacer"),
              _vm._v(" "),
              _c(
                "v-btn",
                {
                  attrs: {
                    color: "orange darken-2",
                    flat: "",
                    disabled: _vm.isDisabled
                  },
                  on: {
                    click: function($event) {
                      $event.stopPropagation()
                      return _vm.sendRegistration($event)
                    }
                  }
                },
                [_vm._v("\n                Зарегестрироваться\n            ")]
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-ebde54cc", module.exports)
  }
}

/***/ })

});