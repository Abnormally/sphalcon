<?php

use Clarity\Support\Phinx\Migration\AbstractMigration;

class User extends AbstractMigration
{
    
    public function up()
    {
        $this->table( 'users' )
            # columns
             ->addColumn( 'email', 'string' )
             ->addColumn( 'password', 'string' )
             ->addColumn( 'nickname', 'string', [ 'null' => true ] )
             ->addColumn( 'token', 'string' )
             ->addColumn( 'activated', 'boolean', [ 'default' => true ] )
            # indexes
             ->addIndex( [ 'email' ], [ 'unique' => true ] )
             ->addIndex( [ 'nickname' ] )
             ->addIndex( [ 'token' ] )
            # created_at and updated_at
             ->addTimestamps()
            # deleted_at
             ->addSoftDeletes()
            # build the entire table
             ->create();
    }
    
    public function down()
    {
        $this->dropTable( 'users' );
    }
    
}
