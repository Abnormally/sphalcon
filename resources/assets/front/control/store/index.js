import Vue from 'vue';
import Vuex from 'vuex';

import Api from './api';
import Tech from './tech';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        api: Api,
        tech: Tech,
    },
    state: {},
    mutations: {},
    actions: {}
});