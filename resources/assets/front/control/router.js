import Vue from 'vue';
import Router from 'vue-router';

import Auth from '$view/layout/Auth';
import Empty from '$view/layout/Empty';

Vue.use(Router);

const router = new Router({
    mode: "history",
    routes: [
        {
            path: '/',
            name: 'home',
            component: () => import('$view/Home'),
        },
        {
            path: '/auth',
            component: Auth,
            children: [
                {
                    path: 'sign-in',
                    name: 'auth.login',
                    component: () => import('$view/auth/SignIn'),
                },
                {
                    path: 'sign-up',
                    name: 'auth.register',
                    component: () => import('$view/auth/SignUp'),
                },
            ]
        },
        {
            path: '*',
            name: 'e404',
            component: () => import('$view/errors/NotFound')
        },
    ]
});

export default router;