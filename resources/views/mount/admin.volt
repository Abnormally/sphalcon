<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Material+Icons">
        <title>Vue mount page</title>
    </head>

    <body>
        <main id="app"></main>
        <script src="{{ base_uri('assets/front/app.js') }}"></script>
    </body>

</html>
