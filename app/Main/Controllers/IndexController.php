<?php

namespace App\Main\Controllers;

class IndexController extends Controller
{
    
    public function index()
    {
        // Take the page, that going to be main entrance to front
        $this->view->pick( 'mount/master' );
    }
    
}
