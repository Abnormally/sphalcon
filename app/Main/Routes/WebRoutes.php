<?php

namespace App\Main\Routes;

use Phalcon\Mvc\Router\Group;

class WebRoutes extends Group
{
    
    public function initialize()
    {
        $this->setPaths(
            [
                'controller' => 'Index',
                'module'     => 'main',
            ]
        );
        
        $this->add( '/', [
            'action' => 'index',
        ], [ 'GET', 'HEAD' ] );
        
        $this->add( '\/(?!(api|ui)(?!\w))\/?.+', [
            'action' => 'index',
        ], [ 'GET', 'HEAD' ] );
    }
    
}
