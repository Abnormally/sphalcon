<?php

/** @var \Phalcon\Mvc\Router $router */
$router = $di->getRouter();

$router
    ->add( '/',
           [
               'controllers' => 'index',
               'action'      => 'index',
           ]
    );

$router->handle();
