<?php

/**
 * We're a registering a set of directories taken from the configuration file
 *
 * @var $config \Phalcon\Config
 * @var $loader \Phalcon\Loader
 */
$loader = ( new \Phalcon\Loader() )->registerDirs(
    [
        $config->application->controllersDir,
        $config->application->modelsDir
    ]
)->register();

if ( empty( $di[ 'loader' ] ) ) {
    $di->setShared( 'loader', function () use ( $loader ) {
        return $loader;
    } );
}
