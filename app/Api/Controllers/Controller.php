<?php

namespace App\Api\Controllers;

use Clarity\Support\Phalcon\Mvc\Controller as BaseController;

abstract class Controller extends BaseController
{
    
    protected static $_connection;
    
    public function initialize()
    {
        $this->view->disable();
    }
    
    /**
     * Check is chosen endpoint is available
     *
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     * @throws \ReflectionException
     */
    public function test()
    {
        $class      = get_called_class();
        $controller = str_replace( 'Controller', '', class_basename( $class ) );
        $folder     = class_basename( dirname( ( new \ReflectionClass( $class ) )->getFileName() ) );
        
        return $this
            ->response(
                [
                    'success'  => true,
                    'endpoint' => $controller,
                    'module'   => $folder,
                ], "Service is active", 248
            );
    }
    
    /**
     * Database connection
     *
     * @return \Phalcon\Db\Adapter\Pdo\Mysql
     */
    protected function dbc()
    {
        if ( !self::$_connection ) {
            /** @var \Clarity\Providers\DB $db */
            $db = $this->db;
            
            /** @var \Phalcon\Db\Adapter\Pdo\Mysql $connection */
            $connection = $db->connection();
            
            $connection->begin();
            
            self::$_connection = $connection;
        }
        
        return self::$_connection;
    }
    
    protected function response( $data, $message = null, $code = 200 )
    {
        return $this
            ->response
            ->sendCookies()
            ->setStatusCode( $code, $message )
            ->setJsonContent( $data );
    }
    
}
