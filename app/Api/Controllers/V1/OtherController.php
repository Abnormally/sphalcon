<?php

namespace App\Api\Controllers\V1;

use App\Api\Controllers\Controller;

class OtherController extends Controller
{
    
    /**
     * Just lmao
     *
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function index()
    {
        return $this->response(
            [
                "This is not api you are looking for"
            ], "Hello, stranger", 418
        );
    }
    
}