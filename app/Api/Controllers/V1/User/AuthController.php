<?php
/**
 * Created by PhpStorm.
 * User: abnormally
 * Date: 20.11.18
 * Time: 15:06
 */

namespace App\Api\Controllers\V1\User;

use App\Api\Controllers\Controller;
use Components\Model\User;

class AuthController extends Controller
{
    
    public function register()
    {
        $data = request()->getJsonRawBody( true );
        
        try {
            ( $user = new User )->create(
                [
                    'nickname'  => $data[ 'nickname' ],
                    'email'     => $data[ 'email' ],
                    'password'  => security()->hash( $data[ 'password' ] ),
                    'token'     => bin2hex( random_bytes( 100 ) ),
                    'activated' => true,
                ]
            );
        } catch ( \Exception $e ) {
            ( $user = User::findFirst( "email = '{$data['email']}'" ) ) ? $user->delete() : null;
            
            return $this->response(
                [
                    'error' => env( 'APP_DEBUG' ) ? $e->getMessage() : $e->getCode(),
                ], "An error occurred", 500
            );
        }
        
        return $this->response(
            [
                "user"  => $user->publicInfo( 'email' ),
                "token" => $user->token,
            ], "Registered", 201
        );
    }
    
    public function login()
    {
        $data = request()->getJsonRawBody( true );
        
        $email    = $data[ 'email' ] ?? null;
        $password = $data[ 'password' ] ?? null;
        
        if ( !$email || !$password ) {
            return $this->response(
                [
                    "message" => "If you building application for this site please contact original developer for info.",
                ], "Lack of data", 406
            );
        }
        
        /** @var User $user */
        $user = User::findFirst( "email = '$email'" );
        if ( !security()->checkHash( $data[ 'password' ], $user->password ) ) {
            return $this->response( null, "Credentials not match", 422 );
        }
        
        $this->dbc();
        $user->token = bin2hex( random_bytes( 100 ) );
        $user->update();
        
        return $this->response(
            [
                "user"  => $user->publicInfo( 'email' ),
                "token" => $user->token,
            ], "You have been authorized"
        );
    }
    
    public function activate()
    {
        // feature
        return $this->response( null, "Not implemented yet", 426 );
    }
    
}
