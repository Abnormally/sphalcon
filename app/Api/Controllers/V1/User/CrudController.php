<?php

namespace App\Api\Controllers\V1\User;

use App\Api\Controllers\Controller;
use Components\Model\User;

class CrudController extends Controller
{
    /**
     * @var int User id from request
     */
    private static $id;
    
    public function initialize()
    {
        parent::initialize();
        
        if ( !self::$id && $this->dispatcher->getParam( 'id' ) ) {
            self::$id = $this->dispatcher->getParam( 'id' );
        }
    }
    
    public function create()
    {
        return $this
            ->response(
                [
                    'user_id' => self::$id,
                ], "User has been created", 201
            );
    }
    
    public function retrieve()
    {
        /** @var User $user */
        $user = User::findFirst( self::$id );
        
        return $this
            ->response(
                [
                    'user' => $user ? $user->publicInfo() : null,
                ],
                $user ? "User has been found" : "User has not been found",
                $user ? 200 : 404
            );
    }
    
    public function update()
    {
        return $this
            ->response(
                [
                    'user_id' => self::$id,
                ], "User has been updated", 202
            );
    }
    
    public function delete()
    {
        return $this
            ->response(
                [
                    'user_id' => self::$id,
                ], "User has been deleted", 205
            );
    }
    
}
