<?php

/* Do not forget to set in each group:
 * /api/v1/{path}
 */

use Composer\Autoload\ClassMapGenerator;

foreach ( ClassMapGenerator::createMap( __DIR__ . '/Routes' ) as $class => $folder ) {
    if ( is_subclass_of( $class, "Phalcon\Mvc\Router\Group" ) ) {
        route()->mount( new $class );
    }
}
