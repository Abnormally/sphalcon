<?php

namespace App\Api;

use Phalcon\Mvc\Router\Group;

abstract class ApiGroup extends Group
{
    
    /**
     * Determines routes for current group
     *
     * @return void
     */
    abstract public function routes();
    
    /**
     * Main controller for current group
     *
     * @return string
     */
    abstract public function controller(): string;
    
    /**
     * Url prefix for group
     *
     * @return string
     */
    abstract public function prefix(): string;
    
    public function initialize()
    {
        $this->setPaths(
            [
                'controller' => $this->controller(),
                'module'     => 'api',
            ]
        );
        
        $this->setPrefix( $this->prefix() );
        
        $this->routes();
        
        $this->add( '/test', [
            'action' => 'test',
        ], [ 'GET' ] );
    }
    
}
