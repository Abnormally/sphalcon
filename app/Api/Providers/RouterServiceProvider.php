<?php

namespace App\Api\Providers;

use Phalcon\Di\FactoryDefault;
use Clarity\Providers\ServiceProvider;
use Clarity\Contracts\Providers\ModuleInterface;

class RouterServiceProvider extends ServiceProvider implements ModuleInterface
{
    
    protected $alias  = 'api';
    protected $shared = false;
    
    /**
     * {@inherit}
     */
    public function register()
    {
        return $this;
    }
    
    /**
     * {@inherit}
     */
    public function module( FactoryDefault $di )
    {
        $di
            ->get( 'dispatcher' )
            ->setDefaultNamespace( 'App\Api\Controllers' );
    }
    
    /**
     * {@inherit}
     */
    public function afterModuleRun()
    {
        require_once realpath( __DIR__ . '/../' ) . '/Routes.php';
    }
    
}
