<?php

namespace App\Api\Routes\V1;

use App\Api\ApiGroup;

class Other extends ApiGroup
{
    
    /**
     * Determines routes for current group
     *
     * @return void
     */
    public function routes()
    {
        $this->add( '', [
            'action' => 'index',
        ] );
    }
    
    /**
     * Main controller for current group
     *
     * @return string
     */
    public function controller(): string
    {
        return "V1\\Other";
    }
    
    /**
     * Url prefix for group
     *
     * @return string
     */
    public function prefix(): string
    {
        return "/api";
    }
    
}