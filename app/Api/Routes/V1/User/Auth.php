<?php

namespace App\Api\Routes\V1\User;

use App\Api\ApiGroup;

class Auth extends ApiGroup
{
    
    public function routes()
    {
        $this->add( '/sign-in', [
            'action' => 'login',
        ], [ 'POST' ] );
        
        $this->add( '/sign-up', [
            'action' => 'register',
        ], [ 'POST' ] );
        
        $this->add( '/activate', [
            'action' => 'activate',
        ], [ 'POST' ] );
    }
    
    public function controller(): string
    {
        return "V1\\User\\Auth";
    }
    
    public function prefix(): string
    {
        return '/api/v1/auth';
    }
    
}
