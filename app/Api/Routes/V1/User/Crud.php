<?php

namespace App\Api\Routes\V1\User;

use App\Api\ApiGroup;

class Crud extends ApiGroup
{
    
    /**
     * Determines routes for current group
     *
     * @return void
     */
    public function routes()
    {
        $this->add( '', [
            'action' => 'create',
        ], [ 'POST' ] );
        
        $this->add( '/{id:[0-9]+}', [
            'action' => 'retrieve',
        ], [ 'GET', 'HEAD' ] );
        
        $this->add( '/{id:[0-9]+}', [
            'action' => 'update',
        ], [ 'PUT', 'PATCH' ] );
        
        $this->add( '/{id:[0-9]+}', [
            'action' => 'delete',
        ], [ 'DELETE' ] );
    }
    
    /**
     * Main controller for current group
     *
     * @return string
     */
    public function controller(): string
    {
        return 'V1\\User\\Crud';
    }
    
    /**
     * Url prefix for group
     *
     * @return string
     */
    public function prefix(): string
    {
        return '/api/v1/user';
    }
    
}
